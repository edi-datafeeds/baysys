EDI_AGNCY_20180614
TableName	Actflag	Created	Changed	AgncyID	RegistrarName	Add1	Add2	Add3	Add4	Add5	Add6	City	CntryCD	Website	Contact1	Tel1	Fax1	Email1	Contact2	Tel2	Fax2	Email2	Depository	State
AGNCY	I	2016/04/27	2016/04/27	33118	Kirkpatrick & Lockhart LLP	Massachusetts						Boston											F	
AGNCY	I	2006/09/06	2006/09/06	1609	Investors Bank & Trust Company	200 Clarendon Street	Boston,	Massachusetts 02116				Boston	US										F	
AGNCY	I	2009/08/19	2009/08/19	5158	The Depository Trust Company.																		F	
AGNCY	I	2015/12/21	2015/12/21	31654	UBS Securities LLC,,.	299 Park Avenue	New York, NY 10171									(203) 719-1075							F	
AGNCY	I	2010/02/01	2010/02/01	6507	Deutsche Bank Trust Company Americas,,																		F	
AGNCY	I	2018/05/26	2018/05/28	39340	The Depository Trust Company..,	Borough of Manhattan	City of New York																F	
AGNCY	I	2009/07/17	2009/07/17	4958	U.S. Bank National Association	100 Wall Street,	Suite 1600,	New York,	NY	10005		New York	US										F	
AGNCY	I	2013/11/23	2013/11/23	24152	Cleary Gottlieb Steen & Hamilton LLP,..,								US										F	
AGNCY	I	2009/10/27	2009/10/27	5630	Deutsche Bank Securities Inc..	60 Wall Street	New York,New York 10005																F	
AGNCY	I	2007/11/15	2007/11/15	3100	Deutsche Bank Trust Company Americas,	60 Wall Street	New York, New York 10005	United States															F	
AGNCY	U	2008/10/14	2009/08/10	3623	The Bank of New York Mellon,																		F	
AGNCY	I	2008/06/14	2008/06/14	3492	Federal Reserve Bank								US										F	
AGNCY	U	2010/11/30	2010/11/30	9164	Sutherland Asbill & Brennan LLP								US										F	
AGNCY	I	2008/05/31	2008/05/31	3482	Nomura Securities International, Inc.																		F	
AGNCY	I	2016/01/26	2016/01/26	32194	Hawkins Delafield & Wood LLP..																		F	
AGNCY	I	2009/01/02	2009/01/02	3960	The Depository Trust Company of New York							New York											F	
AGNCY	U	2008/05/31	2010/09/25	3483	Goldman, Sachs & Co.								US										F	
AGNCY	I	2016/01/06	2016/01/06	31815	The Treasurer of the State of California																		F	
AGNCY	I	2015/12/29	2015/12/29	31717	The Bank of New York Mellon Trust Company, N.A..., .	Virginia						Richmond											F	
AGNCY	I	2016/01/11	2016/01/11	31925	Troutman Sanders LLP	Virginia						Richmond	US										F	
AGNCY	U	2007/12/05	2012/02/09	3151	HSBC Securities (USA) Inc.																		F	
AGNCY	I	2008/09/06	2008/09/06	3594	First Tennessee Bank National Association																		F	
AGNCY	U	2010/11/27	2010/11/30	9019	Sidley Austin LLP	787 Seventh Avenue	New York,	New York 10019				New York	US										F	
AGNCY	I	2010/07/24	2010/07/24	7646	Deutsche Bank Securities Inc...																		F	
AGNCY	I	2017/05/04	2017/05/04	36163	Goldman Sachs & Co LLC																		F	
AGNCY	U	2011/03/10	2011/03/10	11807	Sidley Austin LLP..																		F	
AGNCY	I	2009/02/06	2009/02/06	4240	The Bank of New York Mellon Trust Company N.A.																		F	
AGNCY	I	2011/06/16	2011/06/16	13042	Morgan Stanley & Co. LLC																		F	
AGNCY	I	2009/01/07	2009/01/07	4018	Merrill Lynch Capital Services, Inc																		F	
AGNCY	U	2008/07/11	2012/04/24	3518	Merrill Lynch, Pierce, Fenner & Smith Incorporated																		F	
AGNCY	I	2011/08/24	2011/08/24	13662	McGuireWoods LLP.																		F	
AGNCY	U	2011/04/04	2011/04/04	12142	Hogan Lovells US LLP																		F	
AGNCY	I	2015/04/02	2015/04/02	29531	U.S. Bank National Association.,,.,																		F	
AGNCY	I	2012/05/26	2012/05/26	16462	Clearstream																		F	
AGNCY	I	2012/08/20	2012/08/20	17160	Barclays Bank PLC,.,.																		F	
AGNCY	I	2017/03/03	2017/03/03	35695	Deutsche Bank AG, London Branch.....,																		F	
AGNCY	I	2010/09/25	2010/09/25	8318	J.P. Morgan Securities LLC																		F	
AGNCY	I	2012/03/14	2012/03/14	15527	Davis Polk & Wardwell LLP,,																		F	
AGNCY	I	2007/11/30	2007/11/30	3133	Credit Suisse Securities (USA) LLC																		F	
EDI_ENDOFFILE
